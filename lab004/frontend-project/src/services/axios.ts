import axios from "axios";

const instance = axios.create({
  baseURL: "http://localhost:3000",
});

function delay(time: number) {
  return new Promise((reslove, reject) => {
    setTimeout(() => {
      reslove(true);
    }, time);
  });
}

instance.interceptors.request.use(
  async function (config) {
    // await delay(1000);
    return config;
  },
  function (error) {
    return Promise.reject(error);
  }
);

export default instance;
