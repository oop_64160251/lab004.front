import type User from "@/types/User";
import http from "./axios";

function getUser() {
  return http.get("/users");
}

function saveUser(user: User) {
  return http.post("/users", user);
}

function updateUser(id: number, user: User) {
  return http.patch("/users/" + id, user);
}

function deleteUser(id: number) {
  return http.delete("/users/" + id);
}
export default { getUser, saveUser, updateUser, deleteUser };
