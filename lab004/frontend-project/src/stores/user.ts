import { ref, computed, watch } from "vue";
import { defineStore } from "pinia";
import type User from "@/types/User";
import userService from "@/services/user";
import { useLoadingStore } from "./loading";
import { useMessageStore } from "./message";

export const useUserStore = defineStore("User", () => {
  const loadingStore = useLoadingStore();
  const messageStore = useMessageStore();
  const dialog = ref(false);
  const users = ref<User[]>([]);
  const editedUser = ref<User>({ name: "", login: "", password: "" });

  watch(dialog, (newDialog, oldDialog) => {
    if (!newDialog) {
      editedUser.value = { name: "", login: "", password: "" };
    }
  });
  async function getUser() {
    loadingStore.isLoading = true;
    try {
      const res = await userService.getUser();
      users.value = res.data;
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถดึงข้อมูล User ได้");
    }
    loadingStore.isLoading = false;
  }

  async function saveUser() {
    loadingStore.isLoading = true;
    try {
      if (editedUser.value.id) {
        const res = await userService.updateUser(
          editedUser.value.id,
          editedUser.value
        );
      } else {
        const res = await userService.saveUser(editedUser.value);
      }
      dialog.value = false;
      await getUser();
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถบันทึกข้อมูล User ได้");
    }
    loadingStore.isLoading = false;
  }

  async function deleteUser(id: number) {
    loadingStore.isLoading = true;
    try {
      const res = await userService.deleteUser(id);
      await getUser();
    } catch (e) {
      console.log(e);
      messageStore.showError("ไม่สามารถลบข้อมูล User ได้");
    }
    loadingStore.isLoading = false;
  }
  function editUser(user: User) {
    editedUser.value = JSON.parse(JSON.stringify(user));
    dialog.value = true;
  }
  return {
    users,
    getUser,
    dialog,
    editedUser,
    saveUser,
    editUser,
    deleteUser,
  };
});
